import 'package:flutter/material.dart';

class TopBarProduct extends StatefulWidget {
  @override
  _TopBarProductState createState() => _TopBarProductState();
}

class _TopBarProductState extends State<TopBarProduct> {
  @override
  Widget build(BuildContext context) {
    return Container(
        height: 50,
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: [
            TopBarList(name: "Offer Zone", imageUrl: "product7"),
            TopBarList(name: "Electronics", imageUrl: "product6"),
            TopBarList(name: "Mobiles", imageUrl: "product4"),
            TopBarList(name: "Fashion", imageUrl: "product3"),
            TopBarList(name: "Home", imageUrl: "product5"),
            TopBarList(name: "Beauty", imageUrl: "product1"),
            TopBarList(name: "Food", imageUrl: "product2"),
            TopBarList(name: "Toys & Baby", imageUrl: "product8"),
            TopBarList(name: "Appliance", imageUrl: "product6"),
          ],
        ));
  }
}

class TopBarList extends StatelessWidget {
  String name;
  String imageUrl;

  TopBarList({
    Key key,
    @required this.name,
    @required this.imageUrl,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: 4),
      child: Stack(
        children: [
          ClipRRect(
              borderRadius: BorderRadius.circular(8),
              child: Image.asset('assets/images/' + imageUrl + ".jpg",
                  width: 100, height: 50, fit: BoxFit.cover)),
          Container(
            color: Colors.black26,
            height: 50,
            width: 100,
            alignment: Alignment.center,
            child: Text(
              name,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 15,
                  fontWeight: FontWeight.w400),
            ),
          )
        ],
      ),
    );
  }
}
