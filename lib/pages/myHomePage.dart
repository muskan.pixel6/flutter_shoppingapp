import 'package:flutter/material.dart';
import 'package:carousel_pro/carousel_pro.dart';

import 'popularProduct.dart';
import 'searchPage.dart';
import 'topBarProduct.dart';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    Widget image_carousel = Container(
      height: 200.0,
      child: Carousel(
        boxFit: BoxFit.cover,
        images: [
          AssetImage('assets/images/product1.jpg'),
          AssetImage('assets/images/product2.jpg'),
          AssetImage('assets/images/product3.jpg'),
          AssetImage('assets/images/product4.jpg'),
          AssetImage('assets/images/product5.jpg'),
        ],
        autoplay: true,
        indicatorBgPadding: 5,
        dotColor: Colors.white54,
        dotSize: 6.0,
        animationCurve: Curves.fastOutSlowIn,
        animationDuration: Duration(milliseconds: 1000),
      ),
    );
    return Scaffold(
      appBar: AppBar(
        title: Text("Shopping App"),
        actions: [
          IconButton(icon: Icon(Icons.shopping_cart), onPressed: () {}),
          IconButton(icon: Icon(Icons.login), onPressed: () {})
        ],
      ),
      drawer: Drawer(
        child: ListView(
          children: [
            Padding(
              padding: EdgeInsets.all(15),
              child: Text("Login & Signup"),
            ),
            Divider(color: Colors.grey[350], thickness: 1),
            ListTile(
              title: Text("All Categories"),
              leading: Icon(Icons.category),
              onTap: () {},
            ),
            Divider(color: Colors.grey[350], thickness: 1),
            ListTile(
              title: Text("My orders"),
              leading: Icon(Icons.category_rounded),
              onTap: () {},
            ),
            ListTile(
              title: Text("My Cart"),
              leading: Icon(Icons.shopping_cart),
              onTap: () {},
            ),
            ListTile(
              title: Text("My Account"),
              leading: Icon(Icons.account_circle),
              onTap: () {},
            ),
            ListTile(
              title: Text("My Notification"),
              leading: Icon(Icons.notifications),
              onTap: () {},
            ),
            ListTile(
              title: Text("My chat"),
              leading: Icon(Icons.chat),
              onTap: () {},
            ),
            Divider(
              color: Colors.grey[350],
              thickness: 1,
            ),
            ListTile(
              title: Text("Notification Preferences"),
              onTap: () {},
            ),
            ListTile(
              title: Text("Help Center"),
              onTap: () {},
            ),
            ListTile(
              title: Text("Legal"),
              onTap: () {},
            ),
            Divider(color: Colors.grey[350], thickness: 1),
            ListTile(
              title: Text("Choose Language"),
              leading: Icon(Icons.language),
              onTap: () {},
            ),
          ],
        ),
      ),
      /*  body:SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(height: 10,),
            SearchPage(),
            SizedBox(height: 15,),
            TopBarProduct(),
            SizedBox(height: 10,),
            PopularProduct()
          ],
        ),
      ),*/
      body: ListView(
        children: [
          SizedBox(
            height: 10,
          ),
          SearchPage(),
          SizedBox(
            height: 15,
          ),
          image_carousel,
          SizedBox(
            height: 15,
          ),
          Container(
            padding: EdgeInsets.all(10),
            child: Text(
              "Categories",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w200),
            ),
          ),
          TopBarProduct(),
          SizedBox(
            height: 10,
          ),
          SizedBox(
            height: 10,
          ),
          PopularWidget(),
        ],
      ),
    );
  }
}
